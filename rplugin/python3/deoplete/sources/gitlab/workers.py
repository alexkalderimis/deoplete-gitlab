# ============================================================================
# FILE: workers.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

# pylint: disable=too-many-arguments

import abc
from operator import itemgetter
import threading

from graphql import GraphQLRequest
from candidate import Candidate
from logger import init_logger

logger = init_logger(__name__)

class BackgroundWorker:
  """
  Scaffold for a worker we can run in a background thread
  """

  __metaclass__ = abc.ABCMeta

  def __init__(self, source, endpoint, path, key, token = None):
    self.source = source
    self.endpoint = endpoint
    self.path = path
    self.key = key
    self.token = token

  def start(self):
    thread = threading.Thread(target=self.run, args=())
    thread.daemon = True
    thread.start()

  @abc.abstractmethod
  def query(self):
    raise NotImplementedError()

  @abc.abstractmethod
  def variables(self):
    raise NotImplementedError()

  @abc.abstractmethod
  def from_nodes(self, nodes):
    raise NotImplementedError()

  def run(self):
    logger.debug('Endpoint=%s, Query=%s, Variables=%s', \
        self.endpoint, self.query(), self.variables())

    req = GraphQLRequest(self.endpoint, self.query(), self.variables(), self.path)

    try:
      for nodes in iter(req):
        self.source.found(self.key, set(self.from_nodes(nodes)))

      self.source.done(self.key)
    except Exception as e: # pylint: disable=broad-except
      # Catching all exceptions if fine - we are in the background
      logger.error('Error while searching for %s', self.key, exc_info = e)

class GLLabelsBackgroundWorker(BackgroundWorker):
  """
  BackgroundWorker to fetch labels
  """

  def __init__(self, source, base_url, key, project_path, search_str):
    BackgroundWorker.__init__(self, source, base_url, ['project', 'labels'], key)
    self.project_path = project_path
    self.search_str = search_str

  def from_nodes(self, nodes):
    titles = [{'title': t,
              'escaped': '"{0}"'.format(t) if ' ' in t else t
              } for n in nodes for t in [n['title']]]
    return Candidate('~{escaped}', '~{title}').parse(titles)

  def variables(self):
    return { 'path': self.project_path, 'search_str': self.search_str }

  def query(self):
    query = """
    query($path: ID!, $search_str: String, $after: String) {
      project(fullPath: $path) {
        labels(searchTerm: $search_str, after: $after) {
          pageInfo {
            hasNextPage
            endCursor
          }
          nodes { title }
        }
      }
    }
    """
    return query

class GLUsersBackgroundWorker(BackgroundWorker):
  """
  BackgroundWorker to fetch users
  """
  def __init__(self, source, base_url, key, project_path, search_str):
    BackgroundWorker.__init__(self, source, base_url, ['project', 'members'], key)
    self.project_path = project_path
    self.search_str = search_str

  def from_nodes(self, nodes):
    users = map(itemgetter('user'), nodes)
    by_username = Candidate('@{username}', '{name}')
    return by_username.parse(users)

  def variables(self):
    return { 'path': self.project_path, 'search_str': self.search_str }

  def query(self):
    query = """
    query($path: ID!, $search_str: String, $after: String) {
      project(fullPath: $path) {
        members: projectMembers(search: $search_str, after: $after) {
          pageInfo { endCursor hasNextPage }
          nodes {
            user { username name }
          }
        }
      }
    }
    """
    return query

class GLMergeRequestsBackgroundWorker(BackgroundWorker):
  """
  BackgroundWorker to fetch merge requests
  """

  def __init__(self, source, base_url, key, project_path):
    BackgroundWorker.__init__(self, source, base_url, ['project', 'mrs'], key)
    self.project_path = project_path

  def variables(self):
    return { 'path': self.project_path }

  def from_nodes(self, nodes):
    return Candidate('{reference}', '{title} ({sourceBranch})').parse(nodes)

  def query(self):
    return """
    query($path: ID!, $after: String) {
      project(fullPath: $path) {
        mrs: mergeRequests(state: opened, after: $after) {
          pageInfo { hasNextPage endCursor }
          nodes {
            title
            reference
            sourceBranch
          }
        }
      }
    }
    """

# vim: sw=2 ts=2
