# ============================================================================
# FILE: graphql.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

import datetime
import json
import functools
import hashlib
import requests
from xdg import xdg_cache_home

try:
  from logger import init_logger
except ImportError:
  def init_logger(_name):
    class Logger:
      """Polyfill for missing deoplete logger"""
      def debug(self, *args):
        pass
      def error(self, *args):
        pass

    return Logger()

try:
  import requests_cache.backends.storage.dbdict as dbdict
except ImportError:
  import requests_cache.backends.sqlite as dbdict

logger = init_logger(__name__)

cache_dir = xdg_cache_home() / 'deoplete-gitlab'
cache = cache_dir / 'graphql.sqlite'
logger.debug('Requests cached at %s', cache)

cache_dir.mkdir(parents=True, exist_ok=True)
responses = dbdict.DbDict(str(cache), 'responses', fast_save=True)
expiries = dbdict.DbDict(str(cache), 'expiries', fast_save=True)

def check_expired(key):
  expiry = datetime.datetime.fromtimestamp(expiries[key])
  if expiry < datetime.datetime.now():
    raise ExpiredError

def caching(key, response):
  if response.status_code == 200:
    responses[key] = response.text
    expiries[key] = next_expiry()
  return response

def next_expiry():
  return (datetime.datetime.now() + datetime.timedelta(hours=1)).timestamp()

def many(x):
  if x is None:
    return []
  if isinstance(x, list):
    return x
  return [x]

class ExpiredError(Exception):
  """
  Raised if the cached value is too old
  """

class CachedResponse:
  """
  The minimal interface of a response we need to return from the cache
  """
  def __init__(self, text):
    self.text = text
    self.status_code = 200

  def json(self):
    return json.loads(self.text)

class GraphQLRequest:
  """
  A representation of a GraphQL request, and how to parse it
  """

  def __init__(self, endpoint, query, variables, path, token = None):
    # pylint: disable=too-many-arguments
    self.endpoint = endpoint
    self.query = query
    self.variables = variables
    self.path = path
    self.token = token
    self.after = None
    self.has_next = True

  def __iter__(self):
    self.has_next = True
    self.after = None
    return self

  def __next__(self):
    if not self.has_next:
      raise StopIteration

    has_next, after, results = self.get_next_page(self.after)
    if self.after == after: # have seen this!
      self.has_next = False
    else:
      self.has_next = has_next
      self.after = after

    return results

  def get_path(self, data, path):
    return functools.reduce(self.dig, path, data)

  def dig(self, obj, key):
    if obj is None:
      return None

    if isinstance(obj, list):
      if isinstance(key, int):
        return obj[key]

      return [x for v in obj for x in many(self.dig(v, key))]

    return obj[key] if obj and key in obj else None

  def headers(self):
    headers = {
      'Content-Type': 'application/json',
      'Accept': '*/*',
      'User-Agent': 'neovim-deoplete' # prevents 403
    }
    if self.token is not None:
      headers['Authorization'] = 'Bearer {}'.format(self.token.strip())

    return headers

  def request_with_cache(self, data=''):
    key = '{0}.{1}.{2}'.format(self.endpoint, data, self.token).encode('utf-8')
    key = hashlib.sha256(key).hexdigest()

    try:
      check_expired(key)
      return CachedResponse(responses[key])
    except (KeyError, TypeError, ExpiredError):
      responses.pop(key, None)
      expiries.pop(key, None)

      logger.debug('Sending request to %s', self.endpoint)
      return caching(key, requests.post(self.endpoint, data=data, headers=self.headers()))

  def get_next_page(self, after):
    req_vars = self.variables
    req_vars['after'] = after
    data = json.dumps({'query': self.query, 'variables': req_vars})

    r = self.request_with_cache(data=data)

    if r.status_code == 200:
      response = r.json()
      logger.debug('Response=%s...', r.text[:100])
      errs = response.get('errors', [])

      if errs:
        logger.error('Received graphql errors: %s', [e['message'] for e in errs])
        return (False, None, [])

      results = self.get_path(response, ['data'] + self.path + ['nodes'])
      page_info = self.get_path(response, ['data'] + self.path + ['pageInfo'])
      has_next = page_info.get('hasNextPage', False)
      after = page_info.get('endCursor', None)

      return (has_next, after, results)

    logger.error('GQL request returned status=%s', r.status_code)
    return (False, None, [])

# vim: sw=2 ts=2
