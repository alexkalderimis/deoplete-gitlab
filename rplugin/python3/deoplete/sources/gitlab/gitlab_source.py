# ============================================================================
# FILE: gitlab_source.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

import re
from itertools import islice
from deoplete.base.source import Base

from logger import init_logger

logger = init_logger(__name__)

class Result:
  """
  A result object. Used for managing background workers
  """

  def __init__(self):
    self.candidates = set()
    self.done = False
    self.worker = None

  def add_results(self, results):
    self.candidates = self.candidates | results

class GitlabSource(Base):
  """
  Mixin for GitLab sources

  Manages background workers, and includes common functionality
  """
  # pylint: disable=R0902

  def __init__(self, vim, pattern):
    Base.__init__(self, vim)
    self.mark = '[GL]'
    self.filetypes = ['gitcommit', 'markdown', 'pandoc', 'vimwiki']
    self.dup = False
    self._results = {}
    self.is_volatile = True
    self.min_pattern_length = 3
    self.rank = 800
    self.max_candidates = 0
    self.matchers = ['gitlab_full_match_correcting']
    self.vars = { 'project_path': None }
    self.__pattern = pattern
    self.__origin = None

  def get_complete_position(self, context):
    match = None
    for match in re.finditer(self.__pattern.pattern + '$', context['input']):
      pass

    return match.start() if match is not None else -1

  def start_worker(self, worker, key, args):
    result = self.init_key(key)

    if result.worker is None:
      result.worker = worker(self, self.api_base, key, *args)
      result.worker.start()
      logger.debug('Started worker for %s', key)

  @property
  def origin(self):
    if self.__origin is None and self.get_var('project_path') is None:
      self.__origin = self.vim.eval('fugitive#repo().config("remote.origin.url")')

    return self.__origin

  @property
  def api_base(self):
    """
    Return the API base for the current remote origin
    """

    # We only support gitlab.com URLs for now
    if self.project_path is not None or self.origin.startswith('git@gitlab.com:') or self.origin.startswith('https://gitlab.com/'):
      return 'https://gitlab.com/api/graphql'
    return None

  @property
  def project_path(self):
    """
    Return the Project.full_path for the current repo
    """

    project_path = self.get_var('project_path')
    if project_path is not None:
      return project_path

    # We only support gitlab.com URLs for now
    repo_url = self.origin
    if repo_url and repo_url.startswith('git@gitlab.com:'):
      repo_url = repo_url[len('git@gitlab.com:'):]
    if repo_url and repo_url.startswith('https://gitlab.com/'):
      repo_url = repo_url[len('https://gitlab.com/'):]
    if repo_url and repo_url.endswith('.git'):
      repo_url = repo_url[:-len('.git')]

    return repo_url

  def found(self, key, results):
    logger.debug('Found %s %d results', key, len(results))
    self.init_key(key).add_results(results)
    self.vim.async_call(self.complete)

  def done(self, key):
    logger.debug('Finished loading %s', key)
    self.init_key(key).done = True
    self.vim.async_call(self.complete)

  def complete(self):
    self.vim.eval('deoplete#auto_complete()')

  def is_ready(self, key):
    return key in self._results and self._results[key].done

  def init_key(self, key):
    if key not in self._results:
      self._results[key] = Result()
    return self._results[key]

  def all_results(self):
    results = [c.dict() for r in self._results.values() for c in r.candidates]
    logger.debug('Returning %d results: (first 3: %s)', len(results), list(islice(results, 3)))
    return results

# vim: sw=2 ts=2
