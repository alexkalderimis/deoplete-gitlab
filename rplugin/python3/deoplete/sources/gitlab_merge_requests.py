# ============================================================================
# FILE: gitlab-merge-requests.py
# AUTHOR: Alexis Kalderimis <alex Kalderimis at gmail.com>
# LICENSE: MIT
# ============================================================================

import re

from deoplete.util import load_external_module

load_external_module(__file__, "sources/gitlab")

from workers import GLMergeRequestsBackgroundWorker
from gitlab_source import GitlabSource
from logger import init_logger

logger = init_logger(__name__)

MR_PATTERN = re.compile(r'!([\d]{2,}|[\w\d_-]{3,}|"[^"]{3,}"?)')

class Source(GitlabSource):
  """Fetches merge requests from GitLab API."""

  def __init__(self, vim):
    GitlabSource.__init__(self, vim, MR_PATTERN)

    self.mark = '[MR]'
    self.name = 'gitlab-mrs'
    self.max_menu_width = 100
    logger.debug('Created new gitlab-mrs source')

  def gather_candidates(self, _context):
    if self.api_base is None:
      return []

    key = '{0}:{1}'.format(self.api_base, self.project_path)
    self.start_worker(GLMergeRequestsBackgroundWorker, key, (self.project_path,))

    return self.all_results()

# vim: sw=2 ts=2
