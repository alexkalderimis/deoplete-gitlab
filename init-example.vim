filetype plugin indent on

set nocompatible

" vim-plug plugins
call plug#begin('~/.nvim/plugged')

Plug 'tpope/vim-fugitive'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug '~/projects/deoplete-gitlab'

call plug#end()

let g:deoplete#enable_at_startup = 1
